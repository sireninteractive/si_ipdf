// https://github.com/aheckmann/gm
var fs = require('fs'),
  gm = require('gm'),
  args = process.argv.slice(2),            // store commandline arguments
  
  // user customisable settings
  _settings = {
    bg_color: getArgv('--bg-color') || '#ababab',                   // background colour of output files
    dir: {
      input: getArgv('--input') || __dirname + '/input',            // directory of screencaptures
      output: getArgv('--output') || __dirname + '/output'          // directory to write output files
    },
    padding: {
      horizontal: getArgv('--padding-h') || 100,                    // spacing (px) on left/right of iphone
      vertical: getArgv('--padding-v') || 24                        // spacing (px) below iphone/screencapture
    },
    quality: getArgv('--quality') || 100,                           // image quality of output files
    max_threads: getArgv('--max-threads') || 25,                    // max number of simultaneous images processing
    suppress_logs: getArgv('--suppress') || false                   // don't write the results of composite to console
  },
  
  // don't change these --kk
  _params = {
    delay: {             // timeout offsets when a process is delayed
      min: 150,
      max: 650
    },
    files: {              // metadata about how many files are expected, have been executed, are currently being processed
      count: 0,
      processed: 0,
      running: 0
    },
    iphone: {             // metadata about the iphone frame
      frame: __dirname + '/imgs/iphone5-frame.png',
      width: 773,
      height: 1605,
      viewport: {         // the iphone viewport
        width: 640,
        height: 1136,
        offset: {
          top: 237,
          left: 65
        }
      }
    },
  };

console.time('fileLoop');
console.time('global');

// loop through input directory
fs.readdir(_settings.dir.input, function(err, files){
  if(err) return console.log('ERROR: Could not read input directory');

  if(typeof files === 'object' && files.length > 0){
    var totFiles = files.length;
    for(var i=0; i<totFiles; i++){
      transform(files[i]);
    }
    console.log('Files found: '+totFiles);
    console.timeEnd('fileLoop');
  }
});

function transform(file){
  if(typeof file !== 'string' || file === '' || !/(png|jpg)$/i.test(file)) return console.log('WARNING: Invalid file type ('+file+')');

  // limit the number of images being processed simultaneously
  if(_params.files.running >= _settings.max_threads){
    delay(file);
    return;
  }

  _params.files.count++;
  _params.files.running++;

  getSize(file, function(file, size){
    var rs = fs.createReadStream(_params.iphone.frame),
      outputWidth = _params.iphone.width,
      outputHeight = _params.iphone.height,
      fn = '/'+file,
      g = gm(rs, file);

    // calculate dimensions of output
    outputWidth += (_settings.padding.horizontal * 2);

    if(size.height <= _params.iphone.viewport.height){
      outputHeight += _settings.padding.vertical;
    } else {
      outputHeight = _settings.padding.vertical + size.height + _params.iphone.viewport.offset.top;

      // min height
      if(outputHeight < (_params.iphone.height + _settings.padding.vertical)){
        outputHeight = _params.iphone.height + _settings.padding.vertical;
      }
    }

    g.options({imageMagick: true})
      .background(_settings.bg_color)
      .gravity('North')
      .extent(outputWidth, outputHeight)
      .quality(_settings.quality);

    // write the temporary file to a buffer, before compositing
    g.toBuffer('PNG', function(err, buffer){
        if(err) return handle(err);

        // calculate position for screencapture
        var offsetLeft = _params.iphone.viewport.offset.left + _settings.padding.horizontal,
          offsetTop = _params.iphone.viewport.offset.top;

        // composite the screen capture on top of the iphone frame and write to disk
        gm(buffer, 'img.png')
          .options({imageMagick: true})
          .composite(_settings.dir.input+fn)
          .geometry('+'+offsetLeft+'+'+offsetTop)
          .quality(_settings.quality)
          .write(_settings.dir.output+fn, function(err){
            incProcess();

            var writeEnd = (_params.files.processed >= _params.files.count) ? true : false;

            if(err){
              if(writeEnd){
                console.log('Files transformed: '+_params.files.processed);
                console.timeEnd('global');
              }
              return console.dir(arguments);
            }

            if(_settings.suppress_logs === false){
              console.log(this.outname + ' created :: ' +arguments[3]);
            }

            if(writeEnd){
              console.log('Files transformed: '+_params.files.processed);
              console.timeEnd('global');
            }
          });
      });
  });
}

// returns image dimensions (width, height)
function getSize(file, cb){
  var fn = '/'+file;

  gm(_settings.dir.input+fn)
    .options({imageMagick: true})
    .size(function(err, size){
      if(!err && typeof cb === 'function'){
        // if the image is larger than 640 (iphone 5), bail
        if(size.width > 640){
          console.log('ERROR: Input width greater than 640px ('+file+')');
          incProcess();
          return;
        } else {
          cb(file, size);
        }
      }
    });
}

// offset the transform() function by a random duration
function delay(file){
  setTimeout(function(){
    transform(file);
  }, (Math.random() * (_params.delay.max - _params.delay.min) + _params.delay.min));
}

function incProcess(){
  _params.files.processed++;
  _params.files.running--;
}

// return commandline argument value
function getArgv(flag, hasVal){
		if(typeof flag !== 'string' || flag === '') return;

		if(typeof hasVal !== 'boolean') hasVal = true;

		var ret = null;

    args.forEach(function(val, index, array){
			if(val === flag){
				if(hasVal){
					ret = array[index+1];
				} else {
					ret = true;
				}
			}
		});

		return ret;
	}