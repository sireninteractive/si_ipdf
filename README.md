# Siren Mobile PDF Template Utility

This utility uses Node.js and ImageMagick to insert an iPhone 5 frame around a set of screencaptures from an iPhone 5.

## Requires
* Node.js
* gm (https://github.com/aheckmann/gm)
* ImageMagick (http://www.imagemagick.org/)

## Usage
1. Navigate to the ***si_ipdf*** directory on your computer
2. Copy iPhone screencaptures to the **input** directory. ***Note: Images must be exactly 640px wide, otherwise will be rejected by the utility.***
3. **Simple:** run `npm start`. This command will use all the default options and write processed images to the **output** directory
4. **Advanced:** run `node app.js <options>` using the options listed below, to change the default behaviour.

### Options
The following options can be used on the commandline:

    --bg-color <colour>         Sets the background colour of the outputted image (default: #ababab)
    --input <directory>         Input directory where the screencaptures are stored (default: /input)
    --output <directory>        Output directory to save processed screencaptures (default: /output)
    --padding-h <number>        Spacing on left/right side of iPhone frame; set in px (default: 100)  
    --padding-v <number>        Spacing below iPhone frame; set in px (default: 24)
    --quality <number>          Image quality of processed screencapture; 1 to 100 (default: 100)  
    --max-threads <number>      Maximum number of simultaneously running threads (default: 25)
    --suppress <boolean>        Set to true to suppress output to console everytime a file is written (default: false)


## Installation
1. Download and install Node.js (http://nodejs.org/download/)
2. Download and install ImageMagick (http://www.imagemagick.org/script/binary-releases.php)
3. Clone the repository
4. Run `npm install` to install project dependencies